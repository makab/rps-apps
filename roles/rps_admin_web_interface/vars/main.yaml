---

rps_admin_web_interface_uwsgi_config:
  uid: "{{ rps_admin_web_interface_user }}"
  gid: "{{ rps_admin_web_interface_group }}"
  chdir: "{{rps_admin_web_interface_directory}}/web_interface"
  module: "web_interface.wsgi:application"
  env: DJANGO_SETTINGS_MODULE=web_interface.settings_local
  processes: 5
  harakiri: 600
  max-requests: 5000
  plugin: python3
  buffer-size: 65535

nginx_vhost_name: rps-admin-web-interface
nginx_vhost_server_names:
  - "{{ inventory_hostname }}"
nginx_vhost_custom: |
  client_max_body_size 1M;
  ssl_stapling_verify on;

  location /oauth2/ {
    proxy_pass       http://127.0.0.1:4180;
    proxy_set_header Host                    $host;
    proxy_set_header X-Real-IP               $remote_addr;
    proxy_set_header X-Scheme                $scheme;
    proxy_set_header X-Auth-Request-Redirect $request_uri;
    # or, if you are handling multiple domains:
    # proxy_set_header X-Auth-Request-Redirect $scheme://$host$request_uri;

    proxy_busy_buffers_size   512k;
    proxy_buffers   4 512k;
    proxy_buffer_size   256k;
  }
  location = /oauth2/auth {
    proxy_pass       http://127.0.0.1:4180;
    proxy_set_header Host             $host;
    proxy_set_header X-Real-IP        $remote_addr;
    proxy_set_header X-Scheme         $scheme;
    # nginx auth_request includes headers but not body
    proxy_set_header Content-Length   "";
    proxy_pass_request_body           off;

    proxy_busy_buffers_size   512k;
    proxy_buffers   4 512k;
    proxy_buffer_size   256k;
  }

  location / {
    auth_request /oauth2/auth;
    error_page 401 = /oauth2/sign_in;

    # pass information via X-User and X-Email headers to backend,
    # requires running with --set-xauthrequest flag
    auth_request_set $user   $upstream_http_x_auth_request_user;
    auth_request_set $email  $upstream_http_x_auth_request_email;
    proxy_set_header X-User  $user;
    proxy_set_header X-Email $email;

    # if you enabled --pass-access-token, this will pass the token to the backend
    auth_request_set $token  $upstream_http_x_auth_request_access_token;
    proxy_set_header X-Access-Token $token;

    # if you enabled --cookie-refresh, this is needed for it to work with auth_request
    auth_request_set $auth_cookie $upstream_http_set_cookie;
    add_header Set-Cookie $auth_cookie;

    # When using the --set-authorization-header flag, some provider's cookies can exceed the 4kb
    # limit and so the OAuth2 Proxy splits these into multiple parts.
    # Nginx normally only copies the first `Set-Cookie` header from the auth_request to the response,
    # so if your cookies are larger than 4kb, you will need to extract additional cookies manually.
    auth_request_set $auth_cookie_name_upstream_1 $upstream_cookie_auth_cookie_name_1;

    # Extract the Cookie attributes from the first Set-Cookie header and append them
    # to the second part ($upstream_cookie_* variables only contain the raw cookie content)
    if ($auth_cookie ~* "(; .*)") {
        set $auth_cookie_name_0 $auth_cookie;
        set $auth_cookie_name_1 "auth_cookie_name_1=$auth_cookie_name_upstream_1$1";
    }

    # Send both Set-Cookie headers now if there was a second part
    if ($auth_cookie_name_upstream_1) {
        add_header Set-Cookie $auth_cookie_name_0;
        add_header Set-Cookie $auth_cookie_name_1;
    }

    proxy_buffering on;
    proxy_busy_buffers_size   512k;
    proxy_buffers   4 512k;
    proxy_buffer_size   256k;

    uwsgi_pass unix:/run/uwsgi/app/rps-admin-web-interface/socket;
    proxy_read_timeout 900s;
    proxy_connect_timeout 900s;
    proxy_send_timeout 900s;
    send_timeout 900s;
    uwsgi_read_timeout 900s;
    client_body_timeout 60s;
    include uwsgi_params;
  }
  location /static {
    alias {{rps_admin_web_interface_static_directory}};
  }
nginx_security_headers: false

oauth2_proxy_instance: rps-admin-interface
oauth2_proxy_server_name: "{{ rps_admin_web_interface_server_name }}"
oauth2_proxy_client_id: "{{rps_admin_web_interface_server_name}}"
oauth2_proxy_http_address: 127.0.0.1:4180
oauth2_proxy_client_secret: "{{rps_admin_web_interface_keycloak_client_secret}}"
oauth2_proxy_cookie_secret: "{{rps_admin_web_interface_oauth2_proxy_cookie_secret}}"

oauth2_proxy_show_debug_on_error: true
oauth2_proxy_set_xauthrequest: true

oauth2_proxy_allowed_groups: role:{{rps_admin_web_interface_server_name}}:admin
